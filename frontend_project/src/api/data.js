import axios from 'axios'

export const GetInoviceData = async ({ start, end }) => {
  // code here
  let url = 'http://127.0.0.1:5000/api/invoice?start='+start+'&end='+end
  let x = { data: [] }
  x.data = await axios
    .get(url)
    .then((response) => response.data)
    .catch((error) => console.log(error))
  return x
}
