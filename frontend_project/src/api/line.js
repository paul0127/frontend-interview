import axios from 'axios'
import qs from 'qs'

const config = {
  client_id: '1657266996',
  client_secret: 'd02ab703e15933a8609a270226a328c5',
  redirect_uri: 'http://127.0.0.1:3000/callback',
  client_scope: 'profile%20openid%20email',
}

export const GetLineAuthLink = async () => {
  // code here
  let state = new Date().getTime()
  console.log(state)
  let parameter = new URLSearchParams({
    response_type: 'code',
    client_id: config['client_id'],
    state: state,
  })
  let host = 'https://access.line.me/oauth2/v2.1/authorize'
  let url =
    host +
    '?' +
    parameter.toString() +
    '&scope=' +
    config['client_scope'] +
    '&redirect_uri=' +
    config['redirect_uri'] +
    '&prompt=consent'

  return url
}

export const GetLineAccessToken = async (code) => {
  // code here
  let url = 'https://api.line.me/oauth2/v2.1/token'

  let query = ''
  query += 'grant_type=authorization_code&'
  query += 'code=' + encodeURI(code) + '&'
  query += 'redirect_uri=' + encodeURI(config['redirect_uri']) + '&'
  query += 'client_id=' + encodeURI(config['client_id']) + '&'
  query += 'client_secret=' + encodeURI(config['client_secret'])
  let header = {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Content-Length': query.length,
  }
  let context = {
    http: {
      method: 'POST',
      headers: header,
      body: query,
      ignore_errors: true,
    },
  }
  let res = await fetch(url, context.http).then((response) => response.json())
  return res
}

export const GetLineMeProfile = async (accessToken) => {
  // code here
  let url = 'https://api.line.me/v2/profile'
  let headerData = {
    'Content-Type': 'application/x-www-form-urlencoded',
    charset: 'UTF-8',
    Authorization: 'Bearer ' + accessToken,
  }
  let context = {
    http: {
      method: 'GET',
      headers: headerData,
      ignore_errors: true,
    },
  }

  let res = await fetch(url, context.http).then((response) => response.json())
  localStorage.setItem('token',accessToken)
  return res
}
